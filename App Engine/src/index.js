const express = require("express");
const { PubSub } = require("@google-cloud/pubsub");
const { GoogleAuth } = require("google-auth-library");
const { URL } = require("url");
const jwt = require("jsonwebtoken");

const PORT = process.env.PORT || 3000;
const LOCAL = process.env.NODE_ENV === "dev" ? true : false;
const PREFER_STRAIGHT = process.env.PREFER_STRAIGHT ? true : false;
const ML_ENDPOINT = process.env.ML_ENDPOINT_URL;
const ML_TIMEOUT = process.env.ML_ENDPOINT_TIMEOUT || 400;
const TOPIC = "projects/snakes-nat-2021/topics/Input";

const Board = require("./board.js");
const Point = require("./point.js");

const Wreck = require("@hapi/wreck");
const wreck = Wreck.defaults({});

// Timeout on calls to the secondary service
const wreckWithTimeout = wreck.defaults({ timeout: ML_TIMEOUT });

// Setup PubSub
const pubSubClient = new PubSub();

// Bootstrap server
const app = express();
app.use(express.json());

// Setup REST API
app.get("/", handleIndex);
app.post("/start", handleStart);
app.post("/move", handleMove);
app.post("/end", handleEnd);

// Start server
app.listen(PORT, () =>
  console.log(
    `Snakes N'at Battlesnake Server listening at http://127.0.0.1:${PORT}`
  )
);

const auth = new GoogleAuth();
let client, targetAudience, token;

async function getIdentityToken() {
  try {
    if (token) {
      const decodedToken = jwt.decode(token, {
        complete: true,
      });
      var dateNow = new Date();
      if (!decodedToken || decodedToken.exp < dateNow.getTime() / 1000) {
        token = undefined;
      }
    }
    if (!token) {
      if (!client) {
        if (!targetAudience) {
          targetAudience = new URL(ML_ENDPOINT).origin;
        }
        console.log(
          `request ${ML_ENDPOINT} with target audience ${targetAudience}`
        );
        client = await auth.getIdTokenClient(targetAudience);
      }
      const clientHeaders = await client.getRequestHeaders();
      token = clientHeaders["Authorization"];
    }
    return token;
  } catch (err) {
    throw Error("Failed to create an identity token: ", err);
  }
}

async function _mlAuthOptions(response) {
  const token = await getIdentityToken().catch((err) => {
    console.error(err.message);
    response.status(401).send({
      message: `Internal authorization failed`,
    });
    throw Error("Authentication for cloud run failed: ", err);
  });
  return {
    json: true,
    headers: {
      Authorization: token,
      "Content-Type": "application/json",
    },
  };
}

async function handleML(request, response) {
  try {
    const options = await _mlAuthOptions(response);
    if (request.method === "POST") {
      const payload = JSON.stringify(request.body);
      options.payload = payload;
    }
    const urlPath = request.url;
    const res = await wreckWithTimeout.request(
      request.method,
      `${ML_ENDPOINT}${urlPath}`,
      options
    );
    const body = await Wreck.read(res, options);
    return body;
  } catch (err) {
    console.log("Error response from downstream service " + err);
    response.status(400).send({
      message: "Error response from ml service",
    });
  }
}

/**
 * GET /
 */
async function handleIndex(request, response) {
  console.log("INDEX");

  if (ML_ENDPOINT) {
    const body = await handleML(request, response);
    response.status(200).json(body);
  } else {
    response.status(200).json({
      apiversion: "1",
      author: "steelcity",
      color: PREFER_STRAIGHT ? "#1A73E8" : "#1E8E3E", //PREFER STRAIGHT(BLUE) ELSE (GREEN)
      head: "evil",
      tail: "round-bum",
    });
  }

  publishMessage(TOPIC, {
    type: "INDEX",
  });
}

/**
 * POST /start
 */
async function handleStart(request, response) {
  console.log("START");

  if (ML_ENDPOINT) {
    const body = await handleML(request, response);
    response.status(200).json(body);
  } else {
    response.status(200).send("ok");
  }

  publishMessage(TOPIC, {
    type: "START",
    gameData: request.body,
  });
}

/**
 * POST /move
 */
async function handleMove(request, response) {
  const gameData = request.body;
  console.log(`GAME DATA: ${JSON.stringify(gameData)}`);
  let move;

  // Health limit
  const HEALTH_LIMIT = 25;

  // Create empty board in memory
  const board = new Board(gameData.board, gameData.you);

  // Create point for our snakes head
  const head = new Point(gameData.you.head.x, gameData.you.head.y);

  const previousMove = getPreviousMove(head, gameData.you);

  /**
   * PLACEHOLDER: Call ML Model
   */
  if (ML_ENDPOINT) {
    resp = await handleML(request, response);
    const allPossibleMoves = board.getAllPossibleMoves(head);
    for (var nextMove in allPossibleMoves) {
      if (head.direction(nextMove) === resp.move) {
        move = resp.move;
        break;
      }
    }
    if (!move) {
      console.log(
        `ML suggests ${resp.move} which appears to be invalid, falling back on simplified logic`
      );
    }
  }

  // If ML model is not enable or hasn't predicted a valid move
  if (!move) {
    // If our health is low, start moving towards food
    if (gameData.you.health < HEALTH_LIMIT) {
      const closestFood = board.getClosestFood(head);
      const path = board.getShortestPath(head, closestFood);

      // If we found a path, use first step
      if (path && path.length > 1) {
        move = head.direction(new Point(path[1].x, path[1].y));
      }
    }
  }

  // Last ditch, get a move from all or strategic
  if (!move) {
    // Get any open move
    const allPossibleMoves = board.getAllPossibleMoves(head);
    let nextMove =
      allPossibleMoves[Math.floor(Math.random() * allPossibleMoves.length)];
    move = head.direction(nextMove);

    // Move all opponents forward to all possible moves
    board.advanceOpponents(gameData.you);

    // Get possible moves with open spaces
    const strategicMoves = board.getStrategicMoves(head) || [];

    // Map points to move names
    const nextMoveNames = strategicMoves.map((nextMove) =>
      head.direction(nextMove.shift())
    );

    // If config is to prefer straight and move is avail, do it
    if (
      PREFER_STRAIGHT &&
      previousMove &&
      nextMoveNames &&
      nextMoveNames.includes(previousMove)
    ) {
      move = previousMove;
    } else if (nextMoveNames && nextMoveNames.length) {
      move = nextMoveNames[Math.floor(Math.random() * nextMoveNames.length)];
    }

    LOCAL && board.print();
  }

  // Send the move
  console.log("MOVE: " + move);
  response.status(200).send({ move });

  publishMessage(TOPIC, {
    type: "MOVE",
    gameData,
    move,
  });
}

/**
 * POST /end
 */
async function handleEnd(request, response) {
  console.log("END");

  if (ML_ENDPOINT) {
    const body = await handleML(request, response);
    response.status(200).json(body);
  } else {
    response.status(200).send("ok");
  }

  publishMessage(TOPIC, {
    type: "END",
    gameData: request.body,
  });
}

/**
 * Helper function to publish message to PubSub topic
 * Only send if not local
 */
function publishMessage(topic, message) {
  try {
    const dataBuffer = Buffer.from(JSON.stringify(message));
    if (!LOCAL) {
      pubSubClient.topic(topic).publish(dataBuffer);
      console.log(
        `Message ${JSON.stringify(message)} published to topic ${topic}`
      );
    } else {
      console.log(
        `[LOCAL] Message ${JSON.stringify(message)} published to topic ${topic}`
      );
    }
  } catch (e) {
    console.error(e);
    console.error(`Failed to publish message ${message}.`);
  }
}

function getPreviousMove(head, mySnake) {
  if (mySnake.length > 1) {
    const neck = new Point(mySnake.body[1].x, mySnake.body[1].y);
    const dirToNeck = head.direction(neck);

    switch (dirToNeck) {
      case "up":
        return "down";
      case "down":
        return "up";
      case "left":
        return "right";
      case "right":
        return "left";
    }
  }

  return;
}
