/**
 * Board Class
 */

const Point = require("./point.js");

class Board {
  // Private variables
  _board;
  _boardData;

  // Board Constants
  _OPEN_SPACE = 0;
  _FOOD = 1;
  _HAZARD = 2;
  _SNAKE_START_INDEX = 3;

  _MAX_DEPTH = 8;

  constructor(boardData) {
    this._boardData = boardData;
    this._initializeBoard();
  }

  /**
   * Initialize board with 0s and populate game state
   */
  _initializeBoard() {
    this._board = [];

    const emptyRow = [];
    for (let w = 0; w < this._boardData.width; w++) {
      emptyRow.push(this._OPEN_SPACE);
    }

    // Fill board with empty rows
    for (let i = 0; i < this._boardData.height; i++) {
      this._board.push([...emptyRow]);
    }

    // Add food
    this._boardData.food.forEach((food) => {
      this._board[food.y][food.x] = this._FOOD;
    });

    // Add hazards
    this._boardData.hazards.forEach((hazard) => {
      this._board[hazard.y][hazard.x] = this._HAZARD;
    });

    // Add snakes
    this._boardData.snakes.forEach((snake, index) => {
      snake.body.forEach((bodySection) => {
        this._board[bodySection.y][bodySection.x] =
          this._SNAKE_START_INDEX + index;
      });
    });
  }

  /**
   * Fill all potential single space moves of opponents
   */
  advanceOpponents(userSnake) {
    // Add snakes
    this._boardData.snakes.forEach((snake, index) => {
      // Remove tail
      snake.body.pop();

      // Add all possible moves for snake
      if (snake.id !== userSnake.id) {
        const head = new Point(snake.head.x, snake.head.y);
        const allMoves = this.getAllPossibleMoves(head);
        allMoves.forEach(
          (move) =>
            (this._board[move.y][move.x] = this._SNAKE_START_INDEX + index)
        );
      }
    });
  }

  /**
   * Get all possible moves from given point
   */
  getAllPossibleMoves(point) {
    // Response with longest path
    let possibleMoves = [];

    // Handle down
    this._isOpenSpace(point.up()) && possibleMoves.push(point.up());
    this._isOpenSpace(point.down()) && possibleMoves.push(point.down());
    this._isOpenSpace(point.left()) && possibleMoves.push(point.left());
    this._isOpenSpace(point.right()) && possibleMoves.push(point.right());

    return possibleMoves;
  }

  /**
   * Determine if point exists in path array
   */
  _existsInPath(path, point) {
    const found = path.find((row) => point.equals(row));
    return found ? true : false;
  }

  /**
   * Get a list of strategic moves from given point
   * Logic: Using _MAX_DEPTH as recursive depth, find the longest
   * paths we can move in open spaces
   *
   * Return random longest path
   */
  getStrategicMoves(point, path = [], depth = 0) {
    // Response with longest path
    let maxLength = 0;
    let allPaths = [];

    // Only check so many spaces
    if (depth === this._MAX_DEPTH) {
      return [];
    }

    // Function to test next space and recurse if needed
    const testSpaceAndRecurse = (nextPoint) => {
      if (
        this._isOpenSpace(nextPoint) &&
        !this._existsInPath(path, nextPoint)
      ) {
        const paths = this.getStrategicMoves(
          nextPoint,
          [...path, nextPoint],
          depth + 1
        );

        let updatedPaths = [[nextPoint]];
        if (paths && paths.length) {
          updatedPaths = paths.map((p) => [nextPoint, ...p]);
        }

        updatedPaths.forEach((p) => {
          if (p.length > maxLength) {
            maxLength = p.length;
            allPaths = [p];
          } else if (p.length === maxLength) {
            allPaths.push(p);
          }
        });
      }
    };

    // Test the space and recurse for all directions
    testSpaceAndRecurse(point.up());
    testSpaceAndRecurse(point.down());
    testSpaceAndRecurse(point.left());
    testSpaceAndRecurse(point.right());

    return allPaths;
  }

  /**
   * Test space on board for move,
   * if space value < 2 (0 = open, 1 = food), we can move to it
   */
  _isOpenSpace(point) {
    try {
      if (this._board[point.y][point.x] < 2) {
        return true;
      }

      return false;
    } catch (e) {
      return false;
    }
  }

  /**
   * Get closest food from position
   */
  getClosestFood(point) {
    return point.closest(
      this._boardData.food.map((food) => new Point(food.x, food.y))
    );
  }

  /**
   * A* shortest path
   * https://www.geeksforgeeks.org/a-search-algorithm/
   */
  getShortestPath(start, end) {
    let found;
    const openList = [];
    const closedList = [];

    // Generate starting node
    const startNode = new Point(start.x, start.y);
    const endNode = new Point(end.x, end.y);

    // Initialize openList with starting node
    openList.push(startNode);

    // Loop while we have items in open list
    while (openList.length && !found) {
      let leastF = false;
      let qIndex = false;

      // Find least f index
      openList.forEach((item, index) => {
        if (!leastF || item.f < leastF) {
          qIndex = index;
          leastF = item.f;
        }
      });

      // Remove q from openList
      const q = openList.splice(qIndex, 1).pop();

      // Generate 4 possible moves
      const successors = [];

      const testAndPush = (nextPoint) => {
        if (this._isOpenSpace(nextPoint)) {
          nextPoint.path = [...q.path, { x: q.x, y: q.y }];
          successors.push(nextPoint);
        }
      };

      // Add moves to successors if space is open
      testAndPush(q.up());
      testAndPush(q.down());
      testAndPush(q.left());
      testAndPush(q.right());

      // Loop over successors
      for (const successor of successors) {
        // If endNode, end search
        if (successor.equals(endNode)) {
          successor.path.push({ x: endNode.x, y: endNode.y });
          found = successor;
          break;
        }

        // Set heuristics
        successor.g = q.g + successor.distance(q);
        successor.h = successor.distance(endNode);
        successor.f = successor.g + successor.h;

        // Skip this successor if lesser exists
        if (this._listContainsLesserF(openList, successor)) {
          continue;
        }

        // Skip this successor if lesser exists
        if (this._listContainsLesserF(closedList, successor)) {
          continue;
        }

        openList.push(successor);
      }

      closedList.push(q);
    }

    return (found && found.path) || [];
  }

  _listContainsLesserF(list, point) {
    for (const item of list) {
      if (item.equals(point) && item.f < point.f) {
        return true;
      }
    }

    return false;
  }

  /**
   * Print board
   */
  print() {
    console.log(`-${this._board[0].map(() => "--").join("")}`);
    this._board.forEach((row) => console.log(`|${row.join(" ")}|`));
    console.log(`-${this._board[0].map(() => "--").join("")}`);
  }
}

module.exports = Board;
