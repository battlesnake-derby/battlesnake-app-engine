class Point {
  x;
  y;

  f;
  g;
  h;

  path;

  constructor(x, y) {
    this.x = x;
    this.y = y;

    this.f = 0;
    this.g = 0;
    this.h = 0;

    this.path = [];
  }

  delta(point) {
    const dx = this.x - point.x;
    const dy = this.y - point.y;

    return new Point(dx, dy);
  }

  distance(point) {
    const delta = this.delta(point);

    return Math.abs(delta.x) + Math.abs(delta.y);
  }

  closest(points) {
    let closest;

    points.forEach((point) => {
      if (closest) {
        if (this.distance(point) < this.distance(closest)) {
          closest = point;
        }
      } else {
        closest = point;
      }
    });

    return closest;
  }

  equals(point) {
    return this.x === point.x && this.y === point.y;
  }

  direction(point) {
    const delta = this.delta(point);

    if (delta.x > 0) {
      return "left";
    } else if (delta.x < 0) {
      return "right";
    } else if (delta.y > 0) {
      return "down";
    } else if (delta.y < 0) {
      return "up";
    } else {
      return;
    }
  }

  up() {
    return new Point(this.x, this.y + 1);
  }

  down() {
    return new Point(this.x, this.y - 1);
  }

  left() {
    return new Point(this.x - 1, this.y);
  }

  right() {
    return new Point(this.x + 1, this.y);
  }
}

module.exports = Point;
