# Battlesnake-Overview (Snakes on Cloud - was a suggestion)

## What is Battlesnake? 
Battlesnake is an autonomous survival game. Each Battlesnake is controlled autonomously by a live web server and moves independently attempting to find food, avoid other Battlesnakes, and stay alive as long as possible.

<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/Battlesnake.gif" width="250" height="250">


## How to get started?
To get started you need:
- A Google Cloud Platform (GCP) account
- A Battlesnake account
- Access to a Git Repository or any other version control system to maintain source code. If new to git, learn [Git Basics](https://git-scm.com/book/en/v2/Git-Basics-Getting-a-Git-Repository). <br> 

To participate in a Battlesnake tournament, you need to set up a project to develop and deploy your version of the code. We provide starter frameworks and instructions to help you quickly focus on the areas of the exercise that are most interesting for the objectives (and for competing.) The following steps will help you to set up such a project and the development environment.

* **STEP 1** - Create a GCP account if you do not already have one. You will need to put in credit card details if this is your first time using GCP. Don't worry you will not be billed until you manually upgrade to a free account. The card details are to make sure you're not a bot (Are you??).<br> Finally after creating the account when you login for the first time on the cloud console, you will need to agree to the Terms & Conditions. <br>
<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/Create%20Account.png" width="500" height="500"> <br>
  

<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/Dashboard.png">

Once you have created your account you can proceed to set up your GCP environment. In a Chrome window, login to GCP Console http://console.cloud.google.com
  
This is what the dashboard would look like.
<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/Dashboard_1.png">


* **STEP 2** - Create a new project for Battlesnake
    - In the cloud console dashboard shown above, click on the project selection drop down.
    <img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/Project%20Create.png"> <br>
    - You will see a pop up window to select from a set of existing projects and an option on the top right to create a New Project. Click on *New Project*. <br> 
    <img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/New%20Project.png" width="500" height="500"> <br>
    - Fill out all the necessary details to create project. <br>
        - Project Name
        - Billing Account
        - Organization
        - Location

<br> Depending on your organization you might see different options in project creation. I created a personal GCP account with no parent organization for testing purposes and also used my corp account.<br>

#### Google Corp Account <br>
 <img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/New%20Project-Google.png" width="500" height="500"> <br>
Click on **Select**. Then, click on **Create** to create the new project.
    

#### Personal Account <br>
 <img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/New%20Project-No%20Org.png" width="500" height="500"> <br>
    
* **STEP 3** - Enable Billing and APIs
    - Before you can use the resources in your newly created GCP project, you need to make sure you have attached a billing account to the project and enabled it. Click to [Read more](https://cloud.google.com/billing/docs/how-to/modify-project) about billing accounts.<br> 
    - Depending on your access and use case, you may need to follow the steps mentioned [here](https://cloud.google.com/endpoints/docs/openapi/enable-api) to enable required APIs.
    - To get started with a basic application running in App Engine for your Battlesnake, you can follow the below steps:-
<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/App%20Engine.png" height="700"> <br>

Click on **Create Application** and Select the *Region* and *Service Account*
<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/Create%20App.png"> <br>
Once that is done, you will find yourself on the App Engine Dashboard <br>
<img src="https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/raw/main/App%20Dash.png"> <br>
    

* **STEP 4** - Now that your GCP environment is ready, you can either choose to write your own code or extend one of the existing starter projects using the language of your choice.
 
    - We already have two starter projects which you can use: 
    1) [App Engine](https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/tree/main/App%20Engine) Version - If you want to deploy your snake on App Engine 
    2) [ML Snake](https://gitlab.com/battlesnake-derby/battlesnake-app-engine/-/tree/main/mlsnake) - If you want to use VertexAI to train your snake

* **STEP 5** - If working with a team, you can use the Google Cloud Source Repository for version control or use any other version control of your choice. The advantage of Google Cloud Source Repository is you will have everything at one place in your Google Cloud Account.
* **STEP 6** - Download the [CloudSDK](https://cloud.google.com/sdk/docs/install#linux). <br>
Skip this if you already have installed (you should have got on option to install it when setting up the App Engine Environment in ***STEP 3***)
* **STEP 7** - Set up authentication and register SSH keys
