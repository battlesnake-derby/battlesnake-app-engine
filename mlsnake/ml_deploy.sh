#!/bin/sh

. ./env

IMAGE_NAME=sn-mlsnake-trainer
IMAGE_URI=${REGISTRY}/${PROJECT_ID}/${REPO_NAME}/${IMAGE_NAME}:${COMMIT_HASH}

echo "Pushing ${IMAGE_URI}";
docker push "${IMAGE_URI}";

echo "Deploying ${IMAGE_URI}";

python3 mltrain/create_job_spec.py --image "${IMAGE_URI}" --project "${PROJECT_ID}" --region us-central1
