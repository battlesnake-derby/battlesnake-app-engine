#!/bin/sh

. ./env

IMAGE_NAME=sn-mlsnake
IMAGE_URI=${REGISTRY}/${PROJECT_ID}/${REPO_NAME}/${IMAGE_NAME}:${COMMIT_HASH}

echo "Building ${IMAGE_URI}";

docker build -t "${IMAGE_URI}" -f Dockerfile.deploy .;
