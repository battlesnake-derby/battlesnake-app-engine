
import numpy as np
import torch
from snake_model import load_policy

NUM_LAYERS = 17  # 7 layers indicating the number of alive opponents
LAYER_WIDTH = 23
LAYER_HEIGHT = 23
OBS_SIZE = NUM_LAYERS*LAYER_WIDTH*LAYER_HEIGHT


class Tile:
    def __init__(self, obj):
        self.x = obj['x']
        self.y = obj['y']

    def __str__(self):
        return f'{self.x},{self.y}'

    def get_x(self, head):
        return self.x  # (self.x - head.x) + (LAYER_WIDTH / 2)

    def get_y(self, head):
        return self.y  # (self.y - head.y) + (LAYER_HEIGHT / 2)

    def equals(self, t):
        return self.x == t.x and self.y == t.y


class GameState:
    ''' Current game state parser - converts to an observation to be fed to the policy '''

    def __init__(self, turn, game, board, player):
        self.turn = turn
        self.game = game
        self.board = board
        self.player = player
        self.player_id = self.player['id']
        self.player_size = len(self.player['body'])
        self.head = Tile(self.player['head'])
        self.state = [0] * OBS_SIZE

    def assign(self, xy, layer, val):
        x = xy.get_x(self.head)
        y = xy.get_y(self.head)

        if x >= 0 and x < LAYER_WIDTH and y >= 0 and y < LAYER_HEIGHT:
            idx = int(layer*(LAYER_HEIGHT*LAYER_WIDTH) + x*LAYER_HEIGHT + y)
            self.state[idx] = self.state[idx] + val

    def get_obs(self):
        players = self.board['snakes']
        body = self.player['body']
        playersize = len(body)
        # Assign head_mask
        self.assign(Tile(body[0]), 6, 1)

        alive_count = 0
        for p in players:
            alive_count = alive_count + 1
            # Assign health on head
            self.assign(Tile(p['head']), 0, p['health'])
            backwards_body = reversed(p['body'])
            tail_1 = Tile({'x': 0, 'y': 0})
            tail_2 = Tile({'x': 0, 'y': 0})
            i = 0
            for body_part in backwards_body:
                part = Tile(body_part)
                if i == 0:
                    tail_1 = part
                if i == 1:
                    tail_2 = part
                if tail_1 and tail_2:
                    if tail_1.equals(tail_2):
                        self.assign(part, 7, 1)
                self.assign(part, 1, 1)
                i = i + 1
                self.assign(part, 2, min(i, 255))
                if p['id'] != self.player_id:
                    if len(p['body']) >= self.player_size:
                        # Store the difference
                        self.assign(
                            part, 8, 1 + len(p['body']) - self.player_size)
                    if len(p['body']) < self.player_size:
                        self.assign(part, 9, self.player_size -
                                    len(p['body']))  # Store the difference
            if p['id'] != self.player_id:
                is_larger = 1 if len(p['body']) >= self.player_size else 0
                self.assign(Tile(p['head']), 3, is_larger)

        # Handle the food layer
        food = self.board['food']
        for f in food:
            self.assign(Tile(f), 4, 1)
        # Subtract 1 from alive_count to get the layer index
        alive_count = alive_count - 2
        for x in range(self.board['width']):
            for y in range(self.board['height']):
                self.assign(Tile({'x': x, 'y': y}), 5, 1)
                # Signal how many players are alive
                self.assign(Tile({'x': x, 'y': y}), 10 + alive_count, 1)
        arr = np.array(self.state)
        narr = np.reshape(arr, (-1, LAYER_WIDTH*LAYER_HEIGHT))

        a2 = [np.rot90(np.flipud(np.reshape(narr[layer], (-1, LAYER_WIDTH))), k=3)
              for layer in range(len(narr))]
        res = [0]
        res[0] = a2
        return res
    
    def visualize_board(self, game_data):
        board = game_data['board']
        snakes = board['snakes']
        layers = [np.zeros((23, 23), dtype=np.int32) for _ in range(17)]

        for snake in snakes:
            head = snake['head']
            health = snake['health']
            x = head['x']
            y = head['y']
            layers[0][y][x] = health

        player_len = len(game_data['you']['body'])
        player_id = game_data['you']['id']
        for snake in snakes:
            head = snake['body'][0]
            layers[6][head['y']][head['x']] = 1
            tail_first = snake['body'][::-1] # Reverse the body
            for part_idx in range(len(tail_first)):
                body_part = tail_first[part_idx]
                x = body_part['x']
                y = body_part['y']
                layers[1][y][x] = 1
                layers[2][y][x] = 1 + part_idx
                if snake['id'] != player_id:
                    if len(snake['body']) >= player_len:
                        layers[3][head['y']][head['x']] = 1
                        layers[8][y][x] = 1 + len(snake['body']) - player_len
                    else:
                        layers[9][y][x] = player_len - len(snake['body'])

            tails = tail_first[:2]
            if tails[0]['x'] == tails[1]['x'] and tails[0]['y'] == tails[1]['y']:
                layers[7][tails[0]['y']][tails[1]['x']] = 1

        for food in board['food']:
            layers[4][food['y']][food['x']] = 1

        for y in range(board['height']):
            for x in range(board['width']):
                layers[5][y][x] = 1
                layers[10 + len(snakes) - 2][y][x] = 1

        for lid in range(len(layers)):
            layers[lid] = np.flipud(layers[lid])
            
        return [layers]

class Snake:
    def __init__(self, policy_file, dtype='cpu'):
        self.device = torch.device(dtype)
        self.policy = load_policy(policy_file, self.device)

    def get_move(self, state):
        gs = GameState(state['turn'], state['game'], state['board'], state['you'])
        obs = gs.visualize_board(state)
        inp = torch.tensor(obs, dtype=torch.float32).to(self.device)
        _, action, _, _ = self.policy.act(inp, None, None)
        return action, obs
