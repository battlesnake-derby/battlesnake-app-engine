#!/bin/sh

. ./env

IMAGE_NAME=sn-mlsnake
IMAGE_URI=${REGISTRY}/${PROJECT_ID}/${REPO_NAME}/${IMAGE_NAME}:${COMMIT_HASH}

echo "Pushing ${IMAGE_URI}";
docker push "${IMAGE_URI}";

echo "Deploying ${IMAGE_URI}";
gcloud run deploy mlsnake --image "${IMAGE_URI}" --min-instances 1 --region us-west3 --no-allow-unauthenticated --memory 1Gi
