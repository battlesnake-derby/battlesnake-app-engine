import torch
from a2c_ppo_acktr.model import Policy


class PredictionPolicy(Policy):
    """ Simple class that wraps the packaged policy with the prediction method needed by the gym """

    def __init__(self, obs_shape, act_shape, base=None, base_kwargs=None, device=torch.device('cpu')):
        super(PredictionPolicy, self).__init__(
            obs_shape, act_shape, base, base_kwargs)
        self.device = device

    def predict(self, inputs, deterministic=False):
        # Since this called from our gym environment
        # (and passed as a numpy array), we need to convert it to a tensor
        inputs = torch.tensor(inputs, dtype=torch.float32).to(self.device)
        value, actor_features, rnn_hxs = self.base(inputs, None, None)
        dist = self.dist(actor_features)

        if deterministic:
            action = dist.mode()
        else:
            action = dist.sample()

        return action, value
