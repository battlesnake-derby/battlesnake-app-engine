import torch
from gym.spaces.discrete import Discrete
from .prediction_policy import PredictionPolicy
from .snake_policy import SnakePolicyBase


def load_policy(policy_file, device):
    loaded = torch.load(policy_file, map_location=device)
    policy = create_policy((17, 23, 23),
                           Discrete(4),
                           SnakePolicyBase,
                           device)
    policy.load_state_dict(loaded)
    print(policy)
    return policy


def create_policy(obs_space, act_space, base, device):
    """ Returns a wrapped policy for use in the gym """
    return PredictionPolicy(obs_space, act_space, base=base, device=device)
