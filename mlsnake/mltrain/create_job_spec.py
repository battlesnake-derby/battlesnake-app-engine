# Setup Vertex

from datetime import datetime
import sys
import argparse
import json
from google.cloud.aiplatform import gapic as aip


parser = argparse.ArgumentParser()

parser.add_argument('--region', dest='region',
                    type=str, help='GCP Region.', required=True)
parser.add_argument('--project', dest='project', type=str,
                    help='GCP Project.', required=True)
parser.add_argument('--image', dest='image', type=str,
                    help='Custom training container image.', required=True)

args = parser.parse_args()

image_uri = args.image
project_id = args.project
region = args.region

print(f'Python Version: {sys.version}')
print(f'Image URI: {image_uri}')
print(f'Project ID: {project_id}')
print(f'Region: {region}')

# API service endpoint
API_ENDPOINT = "{}-aiplatform.googleapis.com".format(region)

# Setup the client
client_options = {"api_endpoint": API_ENDPOINT}
client = aip.JobServiceClient(client_options=client_options)
print(client)

# Vertex AI location root path for your dataset, model and endpoint resources
PARENT = f"projects/{project_id}/locations/{region}"

TRAIN_GPU, TRAIN_NGPU = (aip.AcceleratorType.NVIDIA_TESLA_T4, 1)
TRAIN_IMAGE = image_uri
print("Training:", TRAIN_IMAGE, TRAIN_GPU, TRAIN_NGPU)

MACHINE_TYPE = "n1-standard"
VCPU = "16"
TRAIN_COMPUTE = MACHINE_TYPE + "-" + VCPU
print("Train machine type", TRAIN_COMPUTE)

# Build our training job specification

TIMESTAMP = datetime.now().strftime("%Y%m%d%H%M%S")
BUCKET_NAME = "snakes-nat-2021-training"

JOB_NAME = "custom_job_" + TIMESTAMP
MODEL_DIR = "gs://{}/{}".format(BUCKET_NAME, JOB_NAME)
CONTAINER_SPEC = {
    "image_uri": TRAIN_IMAGE,
    "args": ["--model-dir=" + MODEL_DIR],
}

machine_spec = {
    "machine_type": TRAIN_COMPUTE,
    "accelerator_type": TRAIN_GPU,
    "accelerator_count": TRAIN_NGPU,
}

WORKER_POOL_SPEC = [
    {
        "replica_count": 1,
        "machine_spec": machine_spec,
        "container_spec": CONTAINER_SPEC,
    }
]
CUSTOM_JOB = {
    "display_name": JOB_NAME,
    "job_spec": {"worker_pool_specs": WORKER_POOL_SPEC},
}


def create_custom_job(custom_job):
    response = client.create_custom_job(parent=PARENT, custom_job=CUSTOM_JOB)
    print("name:", response.name)
    print("display_name:", response.display_name)
    print("state:", response.state)
    print("create_time:", response.create_time)
    print("update_time:", response.update_time)
    return response.name


print(json.dumps(CUSTOM_JOB))

# Save the job name
JOB_ID = create_custom_job(CUSTOM_JOB)
print(f"Createed job {JOB_ID}")
