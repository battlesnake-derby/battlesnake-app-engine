
try:
    import tensorflow as tf
    from tensorflow.python.client import device_lib
    import argparse
    import os
    import sys
    import re

    import os
    import torch

    from google.cloud import storage
    from tqdm import tqdm
    from functools import partialmethod
    from train import train

    tqdm.__init__ = partialmethod(tqdm.__init__, disable=True)

    device = torch.device('cuda')

    def upload_blob(bucket_name, source_file_name, destination_blob_name):
        """Uploads a file to the bucket."""

        storage_client = storage.Client()
        bucket = storage_client.bucket(bucket_name)
        blob = bucket.blob(destination_blob_name)

        blob.upload_from_filename(source_file_name)

        print(
            "File {} uploaded to {}.".format(
                source_file_name, destination_blob_name
            )
        )

    parser = argparse.ArgumentParser()
    parser.add_argument('--model-dir', dest='model_dir',
                        default=os.getenv("AIP_MODEL_DIR"), type=str, help='Model dir.')
    parser.add_argument('--lr', dest='lr',
                        default=1e-3, type=float,
                        help='Learning rate.')
    parser.add_argument('--num_steps', dest='num_steps',
                        default=600, type=int,
                        help='Number of agents.')
    parser.add_argument('--num_envs', dest='num_envs',
                        default=208, type=int,
                        help='Number of environments.')
    parser.add_argument('--epochs', dest='epochs',
                        default=4, type=int,
                        help='Number of epochs.')
    parser.add_argument('--updates', dest='updates',
                        default=1000, type=int,
                        help='Number of updates.')
    args = parser.parse_args()

    print('Python Version = {}'.format(sys.version))
    print('TensorFlow Version = {}'.format(tf.__version__))
    print('TF_CONFIG = {}'.format(os.environ.get('TF_CONFIG', 'Not found')))
    print('DEVICES', device_lib.list_local_devices())

    num_steps = args.num_steps
    num_envs = args.num_envs
    epochs = args.epochs
    num_updates = args.updates

    print('Num Steps = {}'.format(num_steps))
    print('Num Envs = {}'.format(num_envs))
    print('Num Updates = {}'.format(num_updates))
    print('Epochs = {}'.format(epochs))

    # Number of parallel environments to generate games in
    n_envs = num_envs
    # Number of steps per environment to simulate
    n_steps = num_steps

    policy = train(n_envs, n_steps, num_updates, epochs, device)

    # Save the model
    model_file = "bsg_weights"
    torch.save(policy.state_dict(), f'{model_file}.pt')

    m = re.search(r'gs:\/\/(.*)\/(.*)', args.model_dir)
    bucket_name = m.group(1)
    model_dir = m.group(2)
    upload_blob(bucket_name, f"{model_file}.pt",
                f"{model_dir}/{model_file}.pt")
except:
    print("Trapped fatal error: ", sys.exc_info())
    raise
