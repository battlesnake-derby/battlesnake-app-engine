# Load up our dependencies
import numpy as np
import math
import random
import os
import time
import torch
import torch.nn as nn
import torch.nn.functional as F
import matplotlib.pyplot as plt

from a2c_ppo_acktr.algo import PPO
from a2c_ppo_acktr.model import Policy, NNBase
from a2c_ppo_acktr.storage import RolloutStorage
from collections import deque
from gym_battlesnake.gymbattlesnake import BattlesnakeEnv, Rewards

from snake_model import create_policy, SnakePolicyBase
from tqdm import tqdm


def count_parameters(model):
    return sum(p.numel() for p in model.parameters() if p.requires_grad)

# Let's define a method to check our performance against an older policy
# Determines an unbiased winrate check


def check_performance(current_policy, opponent, n_opponents=3, n_envs=1000, steps=1500, device=torch.device('cpu'), training_rewards=Rewards(win=1, loss=-1, kill=0, eat=0)):
    test_env = BattlesnakeEnv(n_threads=os.cpu_count(), n_envs=n_envs, opponents=[
        opponent for _ in range(n_opponents)], device=device, rewards=training_rewards)
    obs = test_env.reset()
    wins = 0
    losses = 0
    completed = np.zeros(n_envs)
    count = 0
    lengths = []
    win_rewards = []
    loss_rewards = []
    with torch.no_grad():
        # Simulate to a maximum steps across our environments, only recording the first result in each env.
        print("Running performance check")
        for step in tqdm(range(steps)):
            if count == n_envs:
                # Quick break
                print("Check Performance done @ step", step)
                break
            inp = torch.tensor(obs, dtype=torch.float32).to(device)
            action, _ = current_policy.predict(inp, deterministic=True)
            obs, reward, done, info = test_env.step(
                action.cpu().numpy().flatten())
            for i in range(test_env.n_envs):
                if completed[i] == 1:
                    continue  # Only count each environment once
                if 'episode' in info[i]:
                    if info[i]['episode']['w']:
                        completed[i] = 1
                        count += 1
                        wins += 1
                        lengths.append(info[i]['episode']['l'])
                        win_rewards.append(info[i]['episode']['r'])
                    else:
                        completed[i] = 1
                        losses += 1
                        count += 1
                        lengths.append(info[i]['episode']['l'])
                        loss_rewards.append(info[i]['episode']['r'])

    winrate = wins / n_envs
    print("Wins", wins)
    print("Losses", losses)
    print("Average episode length:", np.mean(lengths))
    print("Average reward win:", np.mean(win_rewards))
    print("Average reward loss:", np.mean(loss_rewards))
    return winrate


def train(n_envs, n_steps, num_updates, epochs, device):
    training_rewards = Rewards(win=3, loss=-5, kill=1, eat=4)

    # The gym environment
    env = BattlesnakeEnv(n_threads=1, n_envs=n_envs, rewards=training_rewards)

    # Storage for rollouts (game turns played and the rewards)
    rollouts = RolloutStorage(n_steps,
                              n_envs,
                              env.observation_space.shape,
                              env.action_space,
                              n_steps)
    env.close()

    # Create our policy as defined above
    policy = create_policy(env.observation_space.shape,
                           env.action_space, SnakePolicyBase, device)
    best_old_policy = create_policy(
        env.observation_space.shape, env.action_space, SnakePolicyBase, device)

    # Lets make the old policy the same as the current one
    best_old_policy.load_state_dict(policy.state_dict())

    agent = PPO(policy,
                value_loss_coef=0.5,
                entropy_coef=0.01,
                max_grad_norm=0.5,
                clip_param=0.2,
                ppo_epoch=epochs,
                num_mini_batch=32,
                eps=1e-5,
                lr=1e-3)

    print(f"Trainable Parameters: {count_parameters(policy)}")

    # We'll play 4-way matches
    env = BattlesnakeEnv(n_threads=2, n_envs=n_envs, opponents=[
        policy for _ in range(3)], device=device, rewards=training_rewards)
    obs = env.reset()
    rollouts.obs[0].copy_(torch.tensor(obs))

    # Send our network and storage to the gpu
    policy.to(device)
    best_old_policy.to(device)
    rollouts.to(device)

    # Record mean values to plot at the end
    rewards = []
    value_losses = []
    lengths = []

    start = time.time()
    new_policy_best = False
    for j in range(num_updates):
        episode_rewards = []
        episode_lengths = []
        # Set
        policy.eval()
        print(f"Iteration {j+1}: Generating rollouts")
        for step in tqdm(range(n_steps)):
            with torch.no_grad():
                value, action, action_log_prob, recurrent_hidden_states = policy.act(rollouts.obs[step],
                                                                                     rollouts.recurrent_hidden_states[step],
                                                                                     rollouts.masks[step])
            obs, reward, done, infos = env.step(action.cpu().squeeze())
            obs = torch.tensor(obs)
            reward = torch.tensor(reward).unsqueeze(1)

            for info in infos:
                if 'episode' in info.keys():
                    episode_rewards.append(info['episode']['r'])
                    episode_lengths.append(info['episode']['l'])

            masks = torch.FloatTensor(
                [[0.0] if done_ else [1.0] for done_ in done])
            bad_masks = torch.FloatTensor(
                [[0.0] if 'bad_transition' in info.keys() else [1.0] for info in infos])
            rollouts.insert(obs, recurrent_hidden_states, action,
                            action_log_prob, value, reward, masks, bad_masks)

        with torch.no_grad():
            next_value = policy.get_value(
                rollouts.obs[-1],
                rollouts.recurrent_hidden_states[-1],
                rollouts.masks[-1]
            ).detach()

        # Set the policy to be in training mode (switches modules to training mode for things like batchnorm layers)
        policy.train()

        print("Training policy on rollouts...")
        # We're using a gamma = 0.99 and lambda = 0.95
        rollouts.compute_returns(next_value, True, 0.99, 0.95, False)
        value_loss, action_loss, dist_entropy = agent.update(rollouts)
        rollouts.after_update()

        # Set the policy into eval mode (for batchnorms, etc)
        policy.eval()

        total_num_steps = (j + 1) * n_envs * n_steps
        end = time.time()

        lengths.append(np.mean(episode_lengths))
        rewards.append(np.mean(episode_rewards))
        value_losses.append(value_loss)

        # Every 5 iterations, we'll print out the episode metrics
        if (j+1) % 5 == 0:
            print("\n")
            print("=" * 80)
            print("Iteration", j+1, "Results")
            # Check the performance of the current policy against the prior best
            winrate = check_performance(policy, best_old_policy, device=torch.device(
                device), training_rewards=training_rewards)
            print(f"Winrate vs prior best: {winrate*100:.2f}%")
            print(f"Median Length: {np.median(episode_lengths)}")
            print(f"Max Length: {np.max(episode_lengths)}")
            print(f"Min Length: {np.min(episode_lengths)}")

            # If our policy wins more than 30% of the games against the prior
            # best opponent, update the prior best.
            # Expected outcome for equal strength players is 25% winrate in a 4 player
            # match.
            if winrate > 0.3:
                print("Policy winrate is > 30%. Updating prior best model")
                best_old_policy.load_state_dict(policy.state_dict())
                new_policy_best = True
            else:
                print("Policy has not learned enough yet... keep training!")
            print("-" * 80)

    return policy
